package com.binus;

public class Pattern
{
    public static void main(String[] args)
    {
        PrintTriangle(3);
        PrintRotateTriangle(3);
    }

    private static void PrintTriangle (int line)
    {
        int i;
        String star = "";
        for(i=0;i<line;i++)
        {
            star+="*";
            System.out.println(star);
        }
    }
    private static void PrintRotateTriangle (int line)
    {
        int i;
        String star = "";
        String space = "";
        String printStar = "";
        for(i=0;i<line;i++)
        {
            star+="*";
            space+= " ";
        }
        for(i=0;i<line;i++)
        {
            printStar = space.substring(0,i) + star.substring(i,line);
            System.out.println(printStar);
        }
    }
}
